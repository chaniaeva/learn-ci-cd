const user = {
    firstName: "ayam",
    lastName: "bakar"
}

const additionalInfo = {
    age: 2,
    nationality: "Indonesia"
}

const newUser = {
    ...user,
    ...additionalInfo
}

console.log(newUser);