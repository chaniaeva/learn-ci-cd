const { Articles } = require('../models')
const { nanoid } = require('nanoid')
const BaseController = require('./baseController')

class ArticlesController extends BaseController {
    constructor() {
        super(Articles)
    }
}

module.exports = ArticlesController