1. git init
2. npm init
3. npm install express dotenv pg sequelize
4. Duplicate example.env and edit the duplicated file with desired database credential, then rename it to .env
5. Create .gitignore with:
node_modules
package-lock.json
.env
6. sequelize init
7. Rename config.json -> config.js
use dotenv to use environment variable in sequelize conifg
8. Change model/index.js config variable require to changed config.js
9. sequelize db:create
10. sequelize model:create --name Users --attributes username:string,password:string,fullName:string
11. sequelize model:create --name Articles --attributes content:text,userId:string

## CREATE RELATION
1. Migration user ganti 
- id ke string (22)
    - hapus auto increment
- username string(18)
- fullName string(52)
2. Migration articles ganti
- id string(22)
    - hapus auto increment
- userId string(22)
    - tambah references ke model users dengan key id
3. sequelize db:migrate
4. di model articles tambah
relation hasOne ke model users
5. di model users tambah
relation hasMany ke model articles

## Create API
1. buat index.js
    use dotenv
    inisiasi express
    use express.json middleware
2. buat folder route isi authRoute.js
    isinya /login with return of req body
    /register with return of req body
3. require authRoute.js to index.js & use it as middleware with route prefix
4. buat script start dan dev di package.json

## Set up MCR and auth
1. create controller folder dan userController.js
    - npm i bcrypt nanoid jsonwebtoken
    - add JWT_SECRET to env variable
    - make register dan login method
2. use usercontroller to register & login route in authRoute.js

## Set up authorization
1. create articleController.js with CRUD
2. create articleRoute.js with CRUD
3. require articleRoute to index.js with /article prefix
4. npm i passport passport-jwt
5. create middleware folder dan passportmiddleware.js
    - implement passport jwt middleware
6. use passport middleware to article routes
7. modify post and get article route to use user id in token

## Setup heroku
1. heroku login
1. heroku create app-name
    cek menggunakan git remote -v

cek add on:
3. heroku addons:services
3. heroku addons:services | grep postgres

2. heroku info
    digunakan untuk info heroku
4. heroku addons:create heroku-postgresql
    digunakan untuk menambahkan addon postgres
5. heroku addons --all
    digunakan untuk list addon yang sudah ditambahkan
6. heroku config -s
7. heroku config:set JWT_SECRET="rahasia"
8. heroku config:set PGSSLMODE=no-verify
9. npm i -D sequelize-cli
10. add built script with value sequelize db:migrate
11. edit config.js in production object to use DATABASE_URL environment variable with logging:false
12. git push -f heroku master
