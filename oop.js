const obj = {
    firstName: "Alya",
    lastName: "Chaerul"
}

console.log(obj.firstName);

// Class membutuhkan constructor
// Cara penulisan
class NamaClass {
    // constructor merupakan hal yang dibutuhkan dalam membangun class
    constructor(firstName, lastName, createFirstName) {
        // this adalah sebuah object yang berada di dalam class
        this.firstName = firstName
        this.lastName = lastName
        if (createFirstName) {
            this.halfName = firstName.substring(0, 3)
        }
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`
    }
}

// cara pake class
const chania = new NamaClass("Chania", "Evangelista", false)
// cara menggunakan property this dalam class
console.log(chania.firstName);
console.log(chania.lastName);
console.log(chania.halfName);
console.log(chania.getFullName());

// inheritance
const bapak = {
    emas: true,
    sertifikatRumah: true
}

const anak = {
    ...bapak,
    hp: true
}
console.log(Anak);

class Bapak {
    constructor(emas, sertifikatRumah) {
        this.emas = emas
        this.sertifikatRumah = sertifikatRumah
    }
}

class Anak {
    constructor(hp, emas, sertifikatRumah) {
        super(emas, sertifikatRumah)
        this.hp = hp
    }
}

const anakClass = new Anak(false, true, true)
console.log(anakClass.emas);
console.log(anakClass.sertifikatRumah);
console.log(anakClass.hp);